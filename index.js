#! /usr/bin/env node -r esm

import { createWriteStream } from "fs-extra"
import { stream as got } from "got"
import { Parser } from "htmlparser2"
import { fromPromise } from "most"
import limiter from "most-limiter"
import { fromReadable } from "most-node-streams"
import { basename, extname } from "path"
import pump from "pump"
import { URL } from "url"

fromReadable(process.stdin.setEncoding("utf8"))
  .map(str => str.trim())
  .filter(str => str)
  .thru(limiter(1000))
  .tap(url => console.log("Downloading", url))
  .map(async url => ({
    url,
    name: basename(new URL(url).pathname),
    imageUrl: await getImageUrl(url)
  }))
  .chain(fromPromise)
  .thru(limiter(1000))
  .map(writeImage)
  .chain(fromPromise)
  .forEach(({ url }) => console.log("Downloaded ", url))

function getImageUrl(url) {
  return new Promise((f, r) => {
    const request = got(url)
    const parser = new Parser(
      {
        onopentag(name, attrs) {
          if (
            name === "meta" &&
            attrs.property &&
            attrs.property === "og:image" &&
            attrs.content
          ) {
            request.destroy()
            return f(attrs.content)
          }
        }
      },
      { decodeEntities: true }
    )
    pump(request, parser, err => r(err))
  })
}

async function writeImage(obj) {
  await new Promise((f, r) => {
    pump(
      got(obj.imageUrl, { encoding: null }),
      createWriteStream(`${obj.name}${extname(obj.imageUrl)}`),
      err => (err ? r(err) : f())
    )
  })
  return obj
}
