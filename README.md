# dinsta

Downloads images from Instagram.

## Instructions

Feed Instagram picture URLs on stdin. They will be downloaded in the current directory.

## Installation

```sh
# Use without installing
$ cat urls.txt | npx dinsta

# Install globally (any of these)
$ npm i -g dinsta
$ yarn global add dinsta

# Use when installed
$ cat urls.txt | dinsta
```
